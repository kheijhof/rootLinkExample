#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <cstring>
#include <chrono>
#include <thread>
#include <set>
#include <map>
#include <regex>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <iomanip>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <ctime>
#include <deque>
#include <sstream>
#include <getopt.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <ratio>
#include <random>
#include <utility>

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TPaveLabel.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TDecompSVD.h"
#include "TMatrixDSymEigen.h"
#include "TMacro.h"
#include "TMath.h"
#include "Minuit2/FCNBase.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnMigrad.h"

// using namespace std;

TApplication app("myApp",0,0);

void Wait()
{
	bool runEventLoop = true;
	std::thread thr([&](){
		while(runEventLoop)
		{
			gSystem->ProcessEvents();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
	});
	
	std::cin.get();
	runEventLoop = false;
	thr.join();
}

int main(int argc, char** argv)
{
	for(int i=0; i<10; ++i)
	{
		auto his = new TH1D("h1", "Histogram of random gaussian samples;Sample value;Entries", 120, -6, 6);
		
		his->FillRandom("gaus", 10000);
		his->Draw();
		
		gPad->Update(); // Needed when drawing in the same canvas
		
		Wait(); // Wait for enter to continue
		
		delete his;
	}
	
	// Spam everything
	std::vector<TCanvas*> canvas;
	std::vector<TH1D*> his;
	
	for(int i=0; i<10; ++i)
	{
		auto c = new TCanvas(("canvas"+std::to_string(i)).c_str());
		auto h = new TH1D(("his"+std::to_string(i)).c_str(), "Histogram of random gaussian samples;Sample value;Entries", 120, -6, 6);
		
		h->FillRandom("gaus", 10000);
		
		h->Draw(); 
		
		canvas.push_back(c);
		his.push_back(h);
	}
	
	Wait(); // Wait for enter to continue
	
	// Clean up
	for(auto& c : canvas) delete c;
	for(auto& h : his) delete h;
	canvas.clear();
	his.clear();
	
	return 0;
};
